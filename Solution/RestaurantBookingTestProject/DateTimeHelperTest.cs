﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantBookingSystem.Helpers;

namespace RestaurantBookingTestProject
{
    /// <summary>
    ///This is a test class for DateTimeHelperTest and is intended
    ///to contain all DateTimeHelperTest Unit Tests
    ///</summary>
    [TestClass]
    public class DateTimeHelperTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///A test for Floor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //[HostType("ASP.NET")]
        //[UrlToTest("http://localhost/bookmyseat")]
        public void FloorTest()
        {
            DateTime datetime = DateTime.Now;
            long value = 1000; // 
            DateTimeHelper.DateTimePrecisionLevel precisionLevel = DateTimeHelper.DateTimePrecisionLevel.MilliSeconds;
            var expected = new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute,
                                        datetime.Second, 0);
            DateTime actual = datetime.Floor(value, precisionLevel);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Floor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //[HostType("ASP.NET")]
        //[UrlToTest("http://localhost/bookmyseat")]
        public void FloorTest1()
        {
            DateTime datetime = DateTime.Now;
            DateTimeHelper.DateTimePrecisionLevel precisionLevel = DateTimeHelper.DateTimePrecisionLevel.MilliSeconds;
            var expected = new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute,
                                        datetime.Second, datetime.Millisecond);
            DateTime actual = datetime.Floor(precisionLevel);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Floor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //[HostType("ASP.NET")]
        //[UrlToTest("http://localhost/bookmyseat")]
        public void FloorTest2()
        {
            DateTime datetime = DateTime.Now;
            long value = 12; // 
            DateTimeHelper.DateTimePrecisionLevel precisionLevel = DateTimeHelper.DateTimePrecisionLevel.Hours;
            var expected = new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour - datetime.Hour % 12, 0, 0, 0);
            DateTime actual = datetime.Floor(value, precisionLevel);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Floor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //[HostType("ASP.NET")]
        //[UrlToTest("http://localhost/bookmyseat")]
        public void FloorTest3()
        {
            DateTime datetime = DateTime.Now;
            DateTimeHelper.DateTimePrecisionLevel precisionLevel = DateTimeHelper.DateTimePrecisionLevel.Minutes;
            var expected = new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, 0, 0);
            DateTime actual = datetime.Floor(precisionLevel);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Floor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //[HostType("ASP.NET")]
        //[UrlToTest("http://localhost/bookmyseat")]
        public void FloorTest4()
        {
            DateTime datetime = DateTime.Now;
            long value = 30; // 
            DateTimeHelper.DateTimePrecisionLevel precisionLevel = DateTimeHelper.DateTimePrecisionLevel.Minutes;
            var expected = new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute - datetime.Minute % 30, 0, 0);
            DateTime actual = datetime.Floor(value, precisionLevel);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Floor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //[HostType("ASP.NET")]
        //[UrlToTest("http://localhost/bookmyseat")]
        public void FloorTest5()
        {
            DateTime datetime = DateTime.Now;
            DateTimeHelper.DateTimePrecisionLevel precisionLevel = DateTimeHelper.DateTimePrecisionLevel.Seconds;
            var expected = new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, datetime.Second, 0);
            DateTime actual = datetime.Floor(precisionLevel);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Floor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        //[HostType("ASP.NET")]
        //[UrlToTest("http://localhost/bookmyseat")]
        public void FloorTest6()
        {
            DateTime datetime = DateTime.Now.Floor(30, DateTimeHelper.DateTimePrecisionLevel.Minutes);
            DateTimeHelper.DateTimePrecisionLevel precisionLevel = DateTimeHelper.DateTimePrecisionLevel.Seconds;
            var expected = datetime.Floor(30, DateTimeHelper.DateTimePrecisionLevel.Minutes);
            DateTime actual = datetime.Floor(30, precisionLevel);
            Assert.AreEqual(expected, actual);
        }
    }
}