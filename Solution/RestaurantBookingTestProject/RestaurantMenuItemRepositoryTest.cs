﻿using RestaurantBookingSystem.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace RestaurantBookingTestProject
{
    
    
    /// <summary>
    ///This is a test class for RestaurantMenuItemRepositoryTest and is intended
    ///to contain all RestaurantMenuItemRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RestaurantMenuItemRepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for RestaurantMenuItemRepository Constructor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void RestaurantMenuItemRepositoryConstructorTest()
        {
            RestaurantMenuItemRepository target = new RestaurantMenuItemRepository();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Add
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void AddTest()
        {
            RestaurantMenuItemRepository target = new RestaurantMenuItemRepository(); // TODO: Initialize to an appropriate value
            RestaurantMenuItem item = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Add(item);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Delete
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void DeleteTest()
        {
            RestaurantMenuItemRepository target = new RestaurantMenuItemRepository(); // TODO: Initialize to an appropriate value
            int id = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.Delete(id);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Find
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void FindTest()
        {
            RestaurantMenuItemRepository target = new RestaurantMenuItemRepository(); // TODO: Initialize to an appropriate value
            int id = 0; // TODO: Initialize to an appropriate value
            RestaurantMenuItem expected = null; // TODO: Initialize to an appropriate value
            RestaurantMenuItem actual;
            actual = target.Find(id);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetAllRecord
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        [DeploymentItem("RestaurantBookingSystem.dll")]
        public void GetAllRecordTest()
        {
            IList<RestaurantMenuItem> items = null; // TODO: Initialize to an appropriate value
            SqlConnection cn = null; // TODO: Initialize to an appropriate value
            SqlCommand cmd = null; // TODO: Initialize to an appropriate value
            RestaurantMenuItemRepository_Accessor.GetAllRecord(items, cn, cmd);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetNewMenuItems
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void GetNewMenuItemsTest()
        {
            RestaurantMenuItemRepository target = new RestaurantMenuItemRepository(); // TODO: Initialize to an appropriate value
            int count = 0; // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantMenuItem> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantMenuItem> actual;
            actual = target.GetNewMenuItems(count);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SelectAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void SelectAllTest()
        {
            RestaurantMenuItemRepository target = new RestaurantMenuItemRepository(); // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantMenuItem> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantMenuItem> actual;
            actual = target.SelectAll();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void UpdateTest()
        {
            RestaurantMenuItemRepository target = new RestaurantMenuItemRepository(); // TODO: Initialize to an appropriate value
            RestaurantMenuItem item = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.Update(item);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
