﻿using RestaurantBookingSystem.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace RestaurantBookingTestProject
{
    
    
    /// <summary>
    ///This is a test class for RestaurantTableRepositoryTest and is intended
    ///to contain all RestaurantTableRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RestaurantTableRepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for RestaurantTableRepository Constructor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void RestaurantTableRepositoryConstructorTest()
        {
            RestaurantTableRepository target = new RestaurantTableRepository();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Add
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void AddTest()
        {
            RestaurantTableRepository target = new RestaurantTableRepository(); // TODO: Initialize to an appropriate value
            RestaurantTable table = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Add(table);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Delete
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void DeleteTest()
        {
            RestaurantTableRepository target = new RestaurantTableRepository(); // TODO: Initialize to an appropriate value
            int id = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.Delete(id);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Find
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void FindTest()
        {
            RestaurantTableRepository target = new RestaurantTableRepository(); // TODO: Initialize to an appropriate value
            int id = 0; // TODO: Initialize to an appropriate value
            RestaurantTable expected = null; // TODO: Initialize to an appropriate value
            RestaurantTable actual;
            actual = target.Find(id);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetAllRecord
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        [DeploymentItem("RestaurantBookingSystem.dll")]
        public void GetAllRecordTest()
        {
            IList<RestaurantTable> items = null; // TODO: Initialize to an appropriate value
            SqlConnection cn = null; // TODO: Initialize to an appropriate value
            SqlCommand cmd = null; // TODO: Initialize to an appropriate value
            RestaurantTableRepository_Accessor.GetAllRecord(items, cn, cmd);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetAllRecord
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        [DeploymentItem("RestaurantBookingSystem.dll")]
        public void GetAllRecordTest1()
        {
            IList<object> items = null; // TODO: Initialize to an appropriate value
            SqlConnection cn = null; // TODO: Initialize to an appropriate value
            SqlCommand cmd = null; // TODO: Initialize to an appropriate value
            RestaurantTableRepository_Accessor.GetAllRecord(items, cn, cmd);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ParseDynamicAsRestaurantTable
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        [DeploymentItem("RestaurantBookingSystem.dll")]
        public void ParseDynamicAsRestaurantTableTest()
        {
            RestaurantTableRepository_Accessor target = new RestaurantTableRepository_Accessor(); // TODO: Initialize to an appropriate value
            IEnumerable<object> list = null; // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantTable> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantTable> actual;
            actual = target.ParseDynamicAsRestaurantTable(list);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SelectAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void SelectAllTest()
        {
            RestaurantTableRepository target = new RestaurantTableRepository(); // TODO: Initialize to an appropriate value
            DateTime fromdatetime = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime todatetime = new DateTime(); // TODO: Initialize to an appropriate value
            IDictionary<DateTime, IEnumerable<RestaurantTable>> expected = null; // TODO: Initialize to an appropriate value
            IDictionary<DateTime, IEnumerable<RestaurantTable>> actual;
            actual = target.SelectAll(fromdatetime, todatetime);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SelectAll
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void SelectAllTest1()
        {
            RestaurantTableRepository target = new RestaurantTableRepository(); // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantTable> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<RestaurantTable> actual;
            actual = target.SelectAll();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SelectBookedTables
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void SelectBookedTablesTest()
        {
            RestaurantTableRepository target = new RestaurantTableRepository(); // TODO: Initialize to an appropriate value
            DateTime fromdatetime = new DateTime(); // TODO: Initialize to an appropriate value
            DateTime todatetime = new DateTime(); // TODO: Initialize to an appropriate value
            IDictionary<DateTime, IEnumerable<RestaurantTable>> expected = null; // TODO: Initialize to an appropriate value
            IDictionary<DateTime, IEnumerable<RestaurantTable>> actual;
            actual = target.SelectBookedTables(fromdatetime, todatetime);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/bookmyseat")]
        public void UpdateTest()
        {
            RestaurantTableRepository target = new RestaurantTableRepository(); // TODO: Initialize to an appropriate value
            RestaurantTable table = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.Update(table);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
